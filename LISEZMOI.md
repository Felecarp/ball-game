# Balle
Ball est un jeu en **bac à sable**. Le joueur fait apparaitre des *balles colorées* et les observe *rebondir*, *fusionner* et *se fendre*.
## Installation
1. installer *Python3* et la biblihothèque *[PyGame](https://pygame.org)*
1. executer ball.py avec la commande '''python3 ball.py'''

## Commandes de jeu
- <kbd>&uarr;</kbd>, <kbd>&larr;</kbd>, <kbd>&darr;</kbd> et <kbd>&rarr;</kbd>: *déplacer l'écran*
- <kbd>f</kbd>, <kbd>v</kbd> et <kbd>w</kbd>: faire apparaitre une balle élémentaire, réspectivement *feu*, *végétal* et *eau*, au centre de l'écran
- <kbd>b</kbd>: faire apparaitre une *balle de roche*
- <kbd>Suppr</kbd>: *supprimer* toutes les balles

---

- [version anglaise](README.md)