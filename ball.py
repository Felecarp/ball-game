import pygame.display, pygame.sprite
import sys

pygame.init()

# colors
black = (0, 0, 0)
white = (255, 255, 255)
red = (255, 0, 0)
blue = (0, 0, 255)
green = (0, 255, 0)

class Element:
	def __init__ (self, name, color):
		self.name = name
		self.color = color
		self.super = []
	
	def __str__ (self):
		return "<Element({}, {})>".format(self.name, self.color)

# elements
none_element = Element("none", black)
fire = Element("Fire", red)
vegetable = Element("Vegetable", green)
water = Element("Water", blue)

fire.super.append(vegetable)
vegetable.super.append(water)
water.super.append(fire)

size = (width, height) = (350, 400)

class Ball(pygame.sprite.Sprite):
	def __init__(self, pos = (0, 0), *groups):
		pygame.sprite.Sprite.__init__(self, *groups)
		
		print("Creation d'une nouvelle balle")
		
		self.speed = [2, 2]
		self.color = black
		self.rect = pygame.Rect(pos, (10, 10))
		self.element = none_element
		self.mass = 1
		
		self.set_size((self.mass*2 + 5, self.mass*2 + 5))
	
	def __str__ (self):
		return "<Ball({}, {})>".format(self.element.name, self.mass)
	
	def clone(self):
		return Ball((self.rect.x, self.rect.y))
	
	def update (self, area, ball_group):
		self.rect = self.rect.move(self.speed)
				
		if (self.rect.left <= area.left and self.speed[0] < 0) or \
		(self.rect.right >= area.right and self.speed[0] > 0):
			self.speed[0] = -self.speed[0]
			
		if (self.rect.top <= area.top and self.speed[1] < 0) or \
		(self.rect.bottom >= area.bottom and self.speed[1] > 0):
			self.speed[1] = -self.speed[1]
	
	def set_size (self, size):
		center = self.rect.center
		self.rect.size = size
		self.rect.center = center
		
		self.image = pygame.Surface(size)
		self.image.fill(self.color)
	
	def eat (self, ball):
		print("{} mange {}".format(self, ball))
		self.mass += ball.mass
		self.set_size((self.mass*2 + 5, self.mass*2 + 5))
		
		ball.kill()
	
	def split(self):
		bis = self.clone()
		
		bis.rect.x, bis.rect.y = self.rect.x, self.rect.y
		
		bis.mass = int(self.mass / 2)
		bis.set_size((bis.mass*2 + 5, bis.mass*2 + 5))
		self.mass -= bis.mass
		self.set_size((self.mass*2 + 5, self.mass*2 + 5))
		
		bis.speed = [-self.speed[0], self.speed[1]]
		self.speed = [self.speed[0], -self.speed[1]]
		
		for group in self.groups():
			group.add(bis)
	
class ElementBall(Ball):
	def __init__(self, element = none_element, pos = (0, 0), *groups):
		Ball.__init__(self, pos, groups)
		self.element = element
		self.color = element.color
		
		self.image.fill(self.color)
	
	def clone (self):
		return ElementBall(self.element, (self.rect.x, self.rect.y))
	
	def update (self, area, ball_group):
		Ball.update(self, area, ball_group)
		
		for ball in ball_group:
			if ball.rect.colliderect(self.rect) and ball.element in self.element.super:
				self.eat(ball)

class RockBall(Ball):
	def __init__(self, pos = (0, 0), *groups):
		Ball.__init__(self, pos, groups)
			
	def clone (self):
		return RockBall((self.rect.x, self.rect.y))
	
	def eat (self, ball):
		if ball.mass <= 1:
			self.mass += ball.mass
			self.set_size((self.mass*2 + 5, self.mass*2 + 5))
			ball.kill()
		else:
			ball.mass += self.mass
			ball.split()
			self.kill()
	
	def update (self, area, ball_group):
		Ball.update(self, area, ball_group)
		
		for ball in ball_group:
			if ball.rect.colliderect(self.rect) and ball.element != none_element:
				self.eat(ball)

screen = pygame.display.set_mode((0, 0), (pygame.RESIZABLE))
area = pygame.Rect((0, 0), size)

ball_group = pygame.sprite.Group()
	
ball_group.add(RockBall((area.width / 2, area.height / 2)))

def info ():
	info = str("informations:\nscreen: {}\nball_group: {}".format(screen, ball_group))
	for ball in ball_group:
		info += "\n - "
		info += str(ball)
	return info

def get_visible_area ():
	pos_aff = (max(area.left, 0), max(area.top, 0))
	return pygame.Rect(pos_aff, (area.right, area.bottom))

while True:
	drawed = []
	
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			sys.exit()
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				sys.exit()
			if event.key == pygame.K_i:
				print(info())
			elif event.key == pygame.K_b:
				ball_group.add(RockBall(area.center))
			elif event.key == pygame.K_f:
				ball_group.add(ElementBall(fire, area.center))
			elif event.key == pygame.K_v:
				ball_group.add(ElementBall(vegetable, area.center))
			elif event.key == pygame.K_w:
				ball_group.add(ElementBall(water, area.center))
			elif event.key == pygame.K_DELETE:
				print("suppression de toutes les balles")
				ball_group.empty()
			elif event.key == pygame.K_UP:
				drawed.append(area)
				area = area.move(0, -10)
			elif event.key == pygame.K_RIGHT:
				drawed.append(area)
				area = area.move(10, 0)
			elif event.key == pygame.K_DOWN:
				drawed.append(area)
				area = area.move(0, 10)
			elif event.key == pygame.K_LEFT:
				drawed.append(area)
				area = area.move(-10, 0)
	
	ball_group.update(area, ball_group)
		
	screen.fill(black)
	
	drawed.append(screen.fill(white, area))
	ball_group.draw(screen)
	
	pygame.display.update(drawed)
	
