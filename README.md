# Ball
*Ball* is a **sandbox game**. The player create *colored balls* and watch them to *bounce*, to *merge* and to *split*.

## Installation
1. install *Python3* and *[PyGame](https://pygame.org)* library
1. launch ball.py with comand '''python3 ball.py'''

## Commands
- <kbd>&uarr;</kbd>, <kbd>&larr;</kbd>, <kbd>&darr;</kbd> and <kbd>&rarr;</kbd>: to *move the screen*
- <kbd>f</kbd>, <kbd>v</kbd> and <kbd>w</kbd>: to create *fire ball*, *vegetable ball* or *water ball* at the center of the screen
- <kbd>b</kbd>: to create *rock ball*
- <kbd>Suppr</kbd>: *delete* all balls

---

- [french version](LISEZMOI.md)
